@extends('Admin.layout')

@section("styles")

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">

    <style>


        .input-group .select2-container {
            position: relative;
            z-index: 2;
            float: left;
            width: 80% !important;
            margin-bottom: 0;
            display: table;
            table-layout: fixed;
        }

        .select2-container {
          width: 100% !important;
        }
    </style>
@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header'=>__('general.Create a new Training')])
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs nav-underline" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#basicInfo" role="tab" aria-selected="false"><i class="fa fa-info-circle"></i> {{__('trainings.Basic info')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#media" role="tab" aria-selected="true"><i class="fa fa-picture-o"></i> {{__('trainings.Media')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#detailInfo" role="tab" aria-selected="false"><i class="fa fa-cog"></i> {{__('trainings.Detail info')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#relatedThings" role="tab" aria-selected="false"><i class="fa fa-cog"></i> {{__('trainings.Related things')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#pricing" role="tab" aria-selected="false"><i class="fa fa-money"></i> {{__('trainings.Cena')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#bonuses" role="tab" aria-selected="false"><i class="fa fa-cart-plus"></i> {{__('trainings.Bonusy')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#videos" role="tab" aria-selected="false"><i class="fa fa-video-camera"></i> {{__('trainings.Videos')}}</a>
                        </li>
                    </ul>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="{{route('admin.trainings.store')}}" method="post" id="trainingForm"
                                  enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-body">
                                            {{csrf_field()}}

                                            <div class="tab-content px-1 pt-1">
                                                <div class="tab-pane active" id="basicInfo" role="tabpanel" aria-labelledby="baseIcon-tab21">
                                                    {{--title--}}
                                                    <div class="form-group">
                                                        <input type="hidden" name="id" id="trainingIdInput" value="">
                                                        <label>{{__('trainings.form.title')}}</label>
                                                        <input required type="text" class="form-control" id="title"
                                                               placeholder="{{__('trainings.form.title')}}"
                                                               name="title" value="{{old('title')}}">
                                                    </div>
                                                    {{--subtitle--}}
                                                    <div class="form-group">
                                                        <label>{{__('trainings.form.subtitle')}}</label>
                                                        <textarea required  class="form-control" id="subtitle"
                                                                  placeholder="{{__('trainings.form.subtitle')}}"
                                                                  name="subtitle" value="{{old('subtitle')}}">{{old('subtitle')}}</textarea>
                                                    </div>
                                                    {{--category--}}

                                                  <div class="form-group">
                                                    <label>{{__("trainings.form.Training URL")}}
                                                      <a title="show preview" id="show-preview" class=" show-url-preview"
                                                         href="{{LaravelLocalization::getNonLocalizedURL(route('preview-training',['id' => $availableId ?? 1],false))}}"
                                                         target="_blank">
                                                        <i class="fa fa-external-link-square"></i>
                                                      </a>
                                                    </label>
                                                    <div class="input-group">
                                                      <div class="input-group-prepend">
                                                        <span class="input-group-text" id="span-url">{{LaravelLocalization::getNonLocalizedURL(URL::route('preview-training',['id' => $availableId ?? 1]))}}/</span>
                                                      </div>
                                                      <input required type="text" class="form-control" id="url_slug"
                                                             placeholder="{{__("trainings.form.Training URL")}}"
                                                             name="url_slug" value="{{old('url_slug')}}">
                                                    </div>
                                                  </div>

                                                  {{--meta-title--}}
                                                  <div class="form-group">
                                                    <label>{{__("trainings.form.Meta Title")}}</label>
                                                    <input type="text" class="form-control"
                                                           placeholder="{{__("trainings.form.Meta Title")}}"
                                                           name="meta_title" value="{{old('meta_title')}}">
                                                  </div>

                                                  {{--meta description--}}
                                                  <div class="form-group">
                                                    <label>{{__("trainings.form.Meta description")}}</label>
                                                    <textarea type="text" class="form-control"
                                                              placeholder="{{__("trainings.form.Meta description")}}"
                                                              name="meta_description"
                                                              rows="3">{{old('meta_description')}}</textarea>
                                                  </div>


                                                  <div class="form-group">
                                                        <label>{{__('trainings.form.category')}}</label>
                                                        @foreach($categories as $item)
                                                            <fieldset class="radio">
                                                                <label>
                                                                    <input type="radio"  required name="category" {{ $item->id == old('category') ? 'checked': ''}}
                                                                    value="{{$item->id}}" class="">
                                                                    {{__('trainings.category.'.$item->category_name)}}
                                                                </label>
                                                            </fieldset>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="baseIcon-tab22">
                                                  {{--instructors--}}
                                                  <div class="form-group">
                                                    <label>{{__('trainings.form.instructor')}}</label>
                                                    @if(Auth::user()->isAdmin())
                                                      <select name="instructor" required class="form-control reference-select-box" placeholder="{{__("trainings.form.select an instructor")}}">
                                                        <option value="0" disabled="" selected>{{__("trainings.form.select an instructor")}}</option>
                                                        @foreach($instructors as $instructor)
                                                          <option value="{{$instructor->id}}" >{{$instructor->userDetails->first_name}} {{ $instructor->userDetails->last_name }}</option>
                                                        @endforeach
                                                      </select>
                                                    @elseif(Auth::user()->isInstructor())
                                                      <select readonly="" name="instructor" required class="form-control reference-select-box" placeholder="{{__("trainings.form.select an instructor")}}">
                                                        <option value="{{Auth::user()->id}}" selected >{{Auth::user()->first_name}} {{ Auth::user()->last_name }}</option>
                                                      </select>
                                                    @endif
                                                  </div>

                                                    {{--featured media type--}}
                                                    <div class="form-group">
                                                        <label>{{__('trainings.form.feature_type')}}</label>
                                                        <fieldset class="radio">
                                                            <label>
                                                                <input type="radio" checked required name="feature_type"
                                                                       value="img" class="feature_type">
                                                                {{__('trainings.form.image')}}
                                                            </label>
                                                        </fieldset>
                                                        <fieldset class="radio">
                                                            <label>
                                                                <input type="radio" required name="feature_type" value="vdo"
                                                                       class="feature_type">
                                                                {{__('trainings.form.video')}}
                                                            </label>
                                                        </fieldset>
                                                    </div>

                                                    {{--featured image--}}
                                                    <div class="form-group hide" id="feature-img-container">
                                                        <label>{{__('trainings.form.feature_img')}}</label>
                                                        <input id="featured_image" type="file" class="form-control"
                                                               name="featured_image">
                                                        <img id="featured_image_preview" class="hide height-150 img-thumbnail">
                                                    </div>

                                                    {{--featured video--}}
                                                    <div class="form-group hide" id="feature-vdo-container">
                                                        <label>{{__('trainings.form.feature_video')}}</label>
                                                        <input id="feature_video" type="text" class="form-control" value="{{old('feature_video')}}"
                                                               placeholder="{{__('trainings.form.video_placeholder')}}" name="feature_video">
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="detailInfo" role="tabpanel" aria-labelledby="baseIcon-tab23">
                                                  <div class="row">
                                                    <div class="col-md-6">
                                                      {{--language--}}
                                                      <div class="form-group">
                                                        <label for="language">{{__("trainings.form.Language")}}</label>
                                                        <select required class="custom-select block" name="language"
                                                                id="language">
                                                          <option disabled>{{__("trainings.form.Please select a value")}}</option>
                                                          <option {{ (app()->getLocale() == 'en') ? 'selected' : '' }} value="en">English
                                                          </option>
                                                          <option  {{ (app()->getLocale() == 'cs') ? 'selected' : '' }} value="cs">
                                                            Czech
                                                          </option>
                                                        </select>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      {{--article visibility--}}
                                                      <div class="form-group">
                                                        <label>{{__("trainings.form.Post visibility")}}</label>
                                                        <select required class="custom-select block" name="visibility">
                                                          <option disabled>{{__("trainings.form.Please select a value")}}</option>
                                                          <option @if (old('visibility')) {{ (old('visibility') == '1') ? 'selected' : '' }} @else selected
                                                                  @endif value="1">{{__("trainings.form.Visible to all")}}
                                                          </option>
                                                          <option @if (old('visibility')) {{ (old('visibility') == '2') ? 'selected' : '' }}  @endif value="2">
                                                            {{__("trainings.form.Hidden for public")}}
                                                          </option>

                                                        </select>
                                                      </div>
                                                    </div>
                                                  </div>

                                                    {{--about training--}}
                                                    <div class="form-group">
                                                        <label>{{__('trainings.form.about_training')}}</label>
                                                        <textarea name="body1" id="body1" cols="30" rows="10"
                                                                  placeholder="{{__('trainings.form.about_training')}}"
                                                                  class="form-control summernote-body">{{old('body1')}}</textarea>
                                                    </div>

                                                    {{--What you will learn--}}
                                                    <div class="form-group">
                                                        <label>{{__('trainings.form.will_learn')}}</label>
                                                        <textarea name="body2" id="body2" cols="30" rows="10"
                                                                  placeholder="{{__('trainings.form.will_learn')}}"
                                                                  class="form-control summernote-body">{{old('body2')}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="relatedThings" role="tabpanel" aria-labelledby="baseIcon-tab23">
                                                    {{--references--}}
                                                    <div class="form-group">
                                                        <div class="form-group mb-2 contact-repeater">
                                                            <label>{{__('trainings.form.references')}}</label>
                                                            <div data-repeater-list="repeater-group-reference">
                                                                <div class="input-group mb-1" data-repeater-item>
                                                                    <select name="references" id="" class="form-control" placeholder="{{__("trainings.form.select reference")}}">
                                                                        <option value="0" disabled="" selected>{{__("trainings.form.select reference")}}</option>
                                                                        @foreach($feedbacks as $fb)
                                                                            <option value="{{$fb->feedback_id}}" >{{substr($fb->feedback_description, 0, 100)}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                    <div class="input-group-append">
                                                            <span class="input-group-btn" id="button-addon2">
                                                                <button class="btn btn-danger" type="button" data-repeater-delete><i class="ft-x"></i></button>
                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <button type="button" id="repeater-reference-btn" data-repeater-create class="btn btn-primary">
                                                                <i class="icon-plus4"></i> {{__("trainings.form.Add new reference")}}
                                                            </button>
                                                        </div>

                                                        {{--related trainings--}}
                                                        <div class="form-group mb-2 related-repeater">
                                                            <label>{{__('trainings.form.related_trainings')}}</label>
                                                            <div data-repeater-list="repeater-group-related">
                                                                <div class="input-group mb-1" data-repeater-item>
                                                                    <select name="related_trainings" id="" class="form-control" placeholder="{{__("trainings.form.select training")}}">
                                                                        <option value="0" disabled="" selected>{{__("trainings.form.select training")}}</option>
                                                                        @foreach($userTrainings as $tr)
                                                                            <option value="{{$tr->trainings_id}}">{{substr($tr->trainings_title, 0, 100)}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                    <div class="input-group-append">
                                                            <span class="input-group-btn" id="button-addon2">
                                                                <button class="btn btn-danger" type="button" data-repeater-delete><i class="ft-x"></i></button>
                                                            </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <button type="button" id="repeater-training-btn" data-repeater-create class="btn btn-primary">
                                                                <i class="icon-plus4"></i> {{__("trainings.form.Add new training")}}
                                                            </button>
                                                        </div>

                                                    </div>

                                                  <div class="form-group">
                                                    <label>{{__("trainings.form.Training Tags")}}</label>

                                                    <select class=" form-control" id="tagSelector"
                                                            multiple="multiple" aria-hidden="true"
                                                            name="tags[]">
                                                      @foreach($tags as $tag)
                                                        <option @if(is_array(old('tags')) && in_array($tag->tag_name, old('tags'))) selected
                                                                @endif value="{{$tag->tag_name}}">{{$tag->tag_name}}</option>
                                                      @endforeach
                                                    </select>
                                                  </div>
                                            </div>

                                                <div class="tab-pane" id="pricing" role="tabpanel" aria-labelledby="baseIcon-tab23">
                                                    {{--prices--}}
                                                  {{--prices--}}
                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      <div class="form-group">
                                                        <label>{{__('trainings.form.price_without_vat')}}</label>
                                                        <input required id="price_wo_vat" type="text" class="form-control currency-inputmask price_inputs" value="{{old('price_wo_vat','0.00')}}"
                                                               placeholder="{{__('trainings.form.price_without_vat')}}" name="price_wo_vat">

                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="form-group">
                                                        <label>{{__('trainings.form.currency')}}</label>
                                                        <select required id="currency" class="form-control" value="{{old('currency',config('system_config.default_currency'))}}"
                                                                placeholder="{{__('trainings.form.currency')}}" name="currency">
                                                          <option value="0" disabled>{{__("trainings.form.select a currency")}}</option>
                                                          @foreach($currencies as $currency)
                                                            <option value="{{$currency->id}}" {{ (config('system_config.default_currency') == $currency->id) ? 'selected' : '' }}> {{$currency->name.' ('.$currency->symbol.') - '. $currency->country}}</option>
                                                          @endforeach
                                                        </select>

                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="form-group">
                                                        <label>{{__('trainings.form.vat')}}</label>
                                                        <input  required id="vat" type="text" class="form-control percentage-inputmask price_inputs"
                                                                placeholder="{{__('trainings.form.vat')}}" name="vat" value="{{App\CMSSettingsModel::vat()}}">
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      <div class="form-group">
                                                        <label>{{__('trainings.form.discount')}}</label>
                                                        <input required id="discount" type="text" class="form-control percentage-inputmask price_inputs" value="0%"
                                                               placeholder="{{__('trainings.form.discount')}}" name="discount">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="form-group">
                                                        <label>{{__('trainings.form.price_with_vat')}}</label>
                                                        <input required id="price_with_vat" type="text" class="form-control currency-inputmask" value="{{old('price_with_vat','0.00')}}"
                                                               placeholder="{{__('trainings.form.price_with_vat')}}" name="price_with_vat">
                                                      </div>
                                                    </div>
                                                  </div>

                                                </div>
                                                <div class="tab-pane" id="bonuses" role="tabpanel" aria-labelledby="baseIcon-tab23">
                                                    {{--icons--}}
                                                    <div class="form-group row">
                                                        <label class="col-form-label pl-15"><span class="icon "><i class="material-icons align-middle">video_library</i></span></label>
                                                        <div class="col-md-11">
                                                            <input type="text" id="video_count" class="form-control" placeholder="" name="video_count">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label pl-15" ><span class="icon "><i class="material-icons">list_alt</i></span></label>
                                                        <div class="col-md-11">
                                                            <input type="text" id="document" class="form-control" placeholder="" name="document">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label pl-15" ><span class="icon "><i class="material-icons">shutter_speed</i></span></label>
                                                        <div class="col-md-11">
                                                            <input type="text" id="video_length" class="form-control" placeholder="" name="video_length">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label pl-15" ><span class="icon "><i class="material-icons">grade</i></span></label>
                                                        <div class="col-md-11">
                                                            <input type="text" id="benefit" class="form-control" placeholder="" name="benefit">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-form-label pl-15" ><span class="icon "><i class="material-icons">timer</i></span></label>
                                                        <div class="col-md-11">
                                                            <input type="text" id="course_duration" class="form-control" placeholder="" name="course_duration">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="videos" role="tabpanel" aria-labelledby="baseIcon-tab23">
                                                    <button type="button" class="btn btn-primary" id="addGroup">
                                                        <i class="fa fa-check-square-o"></i> {{__('trainings.general.createGroup')}}
                                                    </button>
                                                    <div class="groups" id="movableGroups">

                                                    </div>
                                                </div>

                                                <div class="form-actions left">
                                                    <button type="reset" class="btn btn-warning mr-1">
                                                        <i class="ft-x"></i> {{__('trainings.general.reset')}}
                                                    </button>

                                                    <a href="{{ URL::previous() }}">
                                                        <button type="button" href="" class="btn btn-warning mr-1">
                                                            <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
                                                        </button></a>
                                                    <button type="button" class="btn btn-primary" id="saveForm">
                                                        <i class="fa fa-check-square-o"></i> {{__('trainings.general.update')}}
                                                    </button>
                                                    <button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                        <i class="fa fa-check-square-o"></i> {{__('trainings.general.updateAndBack')}}
                                                    </button>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ asset("vendors/js/forms/repeater/jquery.repeater.min.js")}}"></script>
    <script src="{{ asset("vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="{{ asset("js/scripts/trainings_scripts.js?v=1")}}"></script>

    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7"></script>
    <script>
      $(document).ready(function () {

        // Drag Handles
        let nestedDrake = null;

        $('#addGroup').on('click', function (e) {
          e.preventDefault();

          Swal({
            title: '{{ __('trainings.general.createGroupTitle') }}',
            input: 'text',
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: false,
            confirmButtonText: '{{ __('trainings.general.createGroupConfirm') }}',
          }).then((result) => {
            if(result.value) {
              $('.groups').append('<div class="card cardWithShadow group">\n' +
                '<div class="card-header">\n' +
                '<input type="hidden" name="group[]" value="'+result.value+'">\n' +
                '<h4 class="card-title">'+result.value+'</h4>\n' +
                '<a class="heading-elements-toggle"><i class="fa fa-ellipsis font-medium-3"></i></a>\n' +
                '        <div class="heading-elements">\n' +
                '<ul class="list-inline mb-0">\n' +
                '<li><button type="button" class="page-link btn-sm addVideo" data-group="'+result.value+'" aria-label="Previous">\n' +
                '<span aria-hidden="true"><i class="fa fa-plus-circle"></i> {{__('trainings.general.insertVideo')}}</span>\n' +
                '</button></li>\n' +
                '<li><button type="button" class="page-link btn-sm editGroup" data-original-name="'+result.value+'" aria-label="Previous">\n' +
                '<span aria-hidden="true"><i class="fa fa-pencil"></i> {{__('trainings.general.edit')}}</span>\n' +
                '</button></li>'+
                '<li style="margin-left: 3px"><button type="button" class="page-link btn-sm deleteGroup" aria-label="Previous">\n' +
                '<span aria-hidden="true"><i class="fa fa-trash"></i> {{__('trainings.general.delete')}}</span>\n' +
                '</button></li>'+
                '<li class="handle" style="margin-left: 3px"><button type="button" class="page-link btn-sm handle" aria-label="Previous">\n' +
                '<span aria-hidden="true" class="handle"><i class="fa fa-arrows handle"></i> {{__('trainings.general.move')}}</span>\n' +
                '</button></li>'+
                '</ul>\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="card-content collapse show">\n' +
                '<div class="card-body">\n' +
                '<div class="list-group">\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>');
            }
          });

        });

        $('body').on('click', '.editGroup', function (e) {
          e.preventDefault();
          let original_name = $(this).data('original-name');
          let heading = $(this).closest('.group').find('.card-title');

          Swal({
            title: '{{ __('trainings.general.editGroupTitle') }}',
            input: 'text',
            inputValue: original_name,
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: false,
            confirmButtonText: '{{ __('trainings.general.saveEditGroupConfirm') }}',
          }).then((result) => {
            if(result.value) {
              $('body').find('input[value="'+original_name+'"]').val(result.value);
              $('body').find('input[name="'+original_name+'[]"]').attr('name', result.value+'[]');
              heading.text(result.value);
            }
          });

        });


        $('body').on('click', '.addVideo', function (e) {
          let container = $(this).closest('.group');
          let group = container.find('input[name="group[]"]').val();
          swal({
            title: '{{ __('trainings.general.selectVideo') }}',
            input: 'select',
            inputOptions: {
              @foreach($videos as $video)
              '{{$video->id}}': '{{ $video->title }}',
              @endforeach
            },
            inputPlaceholder: '{{ __('trainings.general.selectPlaceholder') }}',
            showCancelButton: true,
            inputValidator: function (value) {
              return new Promise(function (resolve, reject) {
                if (value !== '') {
                  resolve();
                } else {
                  reject('You need to select a video');
                }
              });
            }
          }).then(function (result) {
            if (result.value) {
              container.find('.list-group').append('<a href="javascript:void(0);" class="list-group-item list-group-item-action">'+$('.swal2-select option:selected').text()+'<input type="hidden" name="'+group+'[]" value="'+result.value+'"><div class="pull-right"><span aria-hidden="true" class="move" style="cursor: move"><i class="fa fa-arrows move"></i></span><span aria-hidden="true" class="deleteVideo" style="margin-left: 10px"><i class="fa fa-trash"></i></span></div></a>\n')

              var containers = $('.list-group').toArray();
              if(nestedDrake) {
                nestedDrake.destroy();
              }
                nestedDrake = dragula(containers, {
                  moves: function (el, container, handle) {
                    return handle.classList.contains('move');
                  }
                });

                nestedDrake .on('drop', function (el) {
                  console.log("droped");
                  let new_group = $(el).closest('.group').find('input[name="group[]"]').val();
                  $(el).find('input').attr('name', new_group+'[]');
                });
            }
          });
        });

        $('body').on('click', '.deleteVideo', function (e) {
          $(this).closest('.list-group-item').remove();
        });

        $('body').on('click', '.deleteGroup', function (e) {

          Swal({
            title: 'Are you sure?',
            text: "Really delete this group?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
              $(this).closest('.group').remove();
            }
          });
        });

          // Drag Handles
        let drake = dragula([document.getElementById("movableGroups")], {
          moves: function (el, container, handle) {
            return handle.classList.contains('handle');
          }
        });

        drake.on('drop', function (el) {
          console.log("droped");
          let list = $("#left-handles .card");


          let array = [];
          $.each(list, function (key, value) {
            let id = $(value).data("item-id");
            let item = {
              id: id,
              position: key + 1
            };

            array.push(item);

          });


          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          $.ajax({
            data: {
              array
            },
            dataType: 'json',
            method: 'post',
            url: "{{ route("admin.SaveBlocksPosition") }}",
            cache: false,
            success: function (html) {
              toastr.success('Pozice bloků byla úspěšně uložena.', 'Uloženo');
            }
          });
        });


      });
    </script>

@endsection