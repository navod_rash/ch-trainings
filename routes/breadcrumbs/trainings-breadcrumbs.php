<?php

// Dashboard > trainings
Breadcrumbs::for('admin.instructors.index', function ($trail) {
//    $trail->parent('admin.dashboard');
    $trail->push(__('general.Instructors'), route('admin.instructors.index'));
});

// Dashboard > trainings > create
Breadcrumbs::for('admin.instructors.create', function ($trail) {
    $trail->parent('admin.instructors.index');
    $trail->push(__('general.Create Instructor'), route('admin.instructors.create'));
});

// Dashboard > trainings > edit
Breadcrumbs::for('admin.instructors.edit', function ($trail, $instructor) {
    $trail->parent('admin.instructors.index');
    $trail->push(__('general.Edit Instructor'), route('admin.instructors.edit', 1));
});


// Dashboard > trainings
Breadcrumbs::for('admin.trainings', function ($trail) {
//    $trail->parent('admin.dashboard');
    $trail->push(__('general.Trainings'), route('admin.trainings'));
});

// Dashboard > trainings > create
Breadcrumbs::for('admin.trainings.create', function ($trail) {
    $trail->parent('admin.trainings');
    $trail->push(__('general.Create Training'), route('admin.trainings.create'));
});

// Dashboard > trainings > edit
Breadcrumbs::for('admin.trainings.edit', function ($trail, $training) {
    $trail->parent('admin.trainings');
    $trail->push(__('general.Edit training'), route('admin.trainings.edit', $training->trainings_title));
});

