# Changelog

All notable changes to `ch-trainings` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release

## 2.0.0
- extended controllers by core controllers
- publish only routes,views and config 
- moved core concepts into src