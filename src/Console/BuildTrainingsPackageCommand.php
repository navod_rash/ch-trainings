<?php

namespace Creativehandles\ChTrainings\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class BuildTrainingsPackageCommand extends Command
{
    protected $signature = 'creativehandles:build-training-plugin';

    protected $description = 'Build all prerequisites to start the plugin';

    public function handle()
    {
        $plugin = 'Trainings';

        $this->info('Publising vendor directories');
        $this->callSilent('vendor:publish', ['--provider' => 'Creativehandles\ChTrainings\ChTrainingsServiceProvider']);

        $this->info('Migrating tables');
        //run migrations in the plugin
        Artisan::call('migrate --path=app/Plugins/Trainings/Migrations');

        $this->info('Migration complete');

        //seed active plugins table
        if (! DB::table('active_plugins')->where('plugin', $plugin)->first()) {
            DB::table('active_plugins')->insert([
                'plugin'=>$plugin,
            ]);
        }

        $this->info('Good to go!!');
    }
}
