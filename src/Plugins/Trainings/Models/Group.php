<?php
namespace Creativehandles\ChTrainings\Plugins\Trainings\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{

    protected $table = 'training_groups';


    public function videos()
    {
      return $this->hasMany(VideoRelations::class, 'group', 'id');
    }

}