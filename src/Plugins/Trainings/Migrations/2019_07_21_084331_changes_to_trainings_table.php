<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesToTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trainings', function (Blueprint $table) {
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('language')->default('en');
            $table->integer('visibility')->default(1);
            $table->string('url_slug')->nullable();
            $table->integer('currency')->after('trainings_price_with_vat')->nullable();
            $table->decimal('vat')->after('trainings_price_with_vat')->nullable();
            $table->decimal('discount')->after('trainings_price_with_vat')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trainings', function (Blueprint $table) {
            //
        });
    }
}
