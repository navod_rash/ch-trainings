<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('low_trainings_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name');
            $table->text('category_description');
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('low_trainings_category')->insert([
           [
               'id'=>1,
               'category_name'=>"CERTAINTY",
               'category_description'=>"CERTAINTY",
           ],
            [
                'id'=>2,
                'category_name'=>"GROW",
                'category_description'=>"GROW",
            ],
            [
                'id'=>3,
                'category_name'=>"COMMUNITY",
                'category_description'=>"COMMUNITY",
            ],
            [
                'id'=>4,
                'category_name'=>"PROFIT",
                'category_description'=>"PROFIT",
            ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('low_trainings_category');
    }
}
