<?php

namespace Creativehandles\ChTrainings\Http\Controllers\PluginsControllers;

use App\CMSSettingsModel;
use App\Currency;
use Creativehandles\ChTrainings\Plugins\Trainings\Models\TrainingsModel;
use Creativehandles\ChTrainings\Plugins\Trainings\Trainings;
use Creativehandles\ChVideos\Plugins\Videos\Videos;
use App\Traits\FeedbackTrait;
use App\Traits\UploadTrait;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use ReflectionClass;
use Validator;

class TrainingsController extends Controller
{

    use UploadTrait, FeedbackTrait;


    protected $views = 'Admin.Trainings.';
    protected $logprefix = "TrainingsController";
    /**
     * @var Trainings
     */
    private $trainings;
    protected $eventmodel;

    /**
     * @var Videos
     */
    private $videos;

    public function __construct(Trainings $trainings, Videos $videos)
    {
        $this->trainings = $trainings;
        $reflector = new ReflectionClass(TrainingsModel::class);
        $this->eventmodel = $reflector->getShortName();
        $this->videos = $videos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainings = $this->trainings->getAllTrainings(['feedback', 'linkedFeedback', 'reccomendedTrainings']);
        return view($this->views . "index")->with(compact('trainings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $feedbacks = $this->trainings->getAllFeedback();
        $categories = $this->trainings->getAllCategories();
        $userTrainings = $this->trainings->currentAuthorsTrainings();
        $tags = $this->trainings->getAllTags($this->eventmodel);
        $instructors = $this->trainings->getAllInstructors();
        $availableId = $this->trainings->getNextkey();
        $currencies = Currency::all()->sortBy('name');
        $videos = $this->videos->getAllVideosWithoutPagination();
        return view($this->views . "create")->with(compact('userTrainings', 'feedbacks', 'categories', 'tags',
            'instructors', 'availableId', 'currencies', 'videos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get the request
        $formData = $request->all();
        $validator = $this->validateTrainingForm($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Trainings data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

    $formattedForm = [];

    $formattedForm = $this->getFormattedData($request, $formattedForm);

    try {

        if($request->get('id',null)){
            $formattedForm['trainings_id'] = $request->get('id');
            $storedTraining = $this->trainings->createOrUpdateTraining($formattedForm);
        }else{
            //save it in the database
            $storedTraining = $this->trainings->createOrUpdateTraining($formattedForm);
        }


            $tags = $this->processTags($request);

            if ($storedTraining) {
              // save the video groups
              $this->trainings->saveGroups($request, $storedTraining->trainings_id);

                //prepare references
                $val = $this->processFeedback($request, $storedTraining);

                //prepare recommended trainings
                $this->processReccommendedTrainings($request, $storedTraining);

                //link tags
                if ($tags) {
                    $taglist=[];
                    foreach ($tags as $key=>$value){
                        $taglist[$value]=[
                            'model'=>$this->eventmodel,
                            'ordinal'=>$key+1

                        ];
                    }

                    $storedTraining->tags()->where('model',$this->eventmodel)->sync($taglist);
                }else{
                    $storedTraining->tags()->where('model',$this->eventmodel)->detach();
                }

            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error',
                    'Error Occured :' . $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => "New Training created successfully! ", 'data' => $storedTraining], 200);
        } else {
            return redirect()->route('admin.trainings')->with('success', 'New Training created successfully!');
        }


  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = $this->trainings->getAllCategories();
        $training = $this->trainings->findTrainingById($id, ['groups']);
        $feedbacks = $this->trainings->getAllFeedback();
        $userTrainings = $this->trainings->currentAuthorsTrainings($id);
        $tags = $this->trainings->getAllTags($this->eventmodel);
        $instructors = $this->trainings->getAllInstructors();
        $currencies = Currency::all()->sortBy('name');
        $videos = $this->videos->getAllVideosWithoutPagination();
        return view($this->views . "edit")->with(compact('training', 'feedbacks', 'userTrainings', 'categories', 'tags',
            'instructors', 'currencies', 'videos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //get the request
        $formData = $request->all();

        $validator = $this->validateTrainingForm($request);

        if ($validator->fails()) {
            Log::error($this->logprefix . '(' . __FUNCTION__ . '): Trainings data incomplete ', $request->all());

            if ($request->ajax()) {
                return response()->json(['msg' => $validator->errors()->toArray()], 400);
            } else {
                return redirect()->back()->withInput()->withErrors($validator);
            }
        }

        $formattedForm = [];

        $formattedForm = $this->getFormattedData($request, $formattedForm);

        //set training id
        $formattedForm["trainings_id"] = $id;

        try {

            //save it in the database
            $storedTraining = $this->trainings->createOrUpdateTraining($formattedForm);

            $tags = $this->processTags($request);

            if ($storedTraining) {

                // save the video groups
                $this->trainings->saveGroups($request, $storedTraining->trainings_id, true);

                $this->processFeedback($request, $storedTraining);
                //prepare recommended trainings
                $this->processReccommendedTrainings($request, $storedTraining);

                //link tags
                if ($tags) {
                    $taglist = [];
                    foreach ($tags as $key => $value) {
                        $taglist[$value] = [
                            'model' => $this->eventmodel,
                            'ordinal'=>$key+1
                        ];
                    }
                    $storedTraining->tags()->where('model', $this->eventmodel)->sync($taglist);
                } else {
                    $storedTraining->tags()->where('model', $this->eventmodel)->detach();
                }
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => "Error Occured", 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error',
                    'Error Occured :' . $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => "Training Updated successfully!", 'data' => $storedTraining], 200);
        } else {
            return redirect()->route('admin.trainings')->with('success', 'Training Updated successfully!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletedTraining = $this->trainings->deleteTrainingById($id);

        if (!$deletedTraining) {
            return response()->json(['msg' => "Error Occured"], 400);
        }

        return response()->json(['msg' => "Page Deleted"], 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function validateTrainingForm(Request $request)
    {
        $rules = [
            'title' => 'required',
            'instructor' => 'required',
            'subtitle' => 'required',
            'category' => 'required',
            'body1' => 'required',
            'price_wo_vat' => 'required',
            'price_with_vat' => 'required',
        ];


        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

    /**
     * @param Request $request
     * @param $formattedForm
     * @return mixed
     */
    protected function getFormattedData(Request $request, $formattedForm)
    {
        $formattedForm["url_slug"] = ($request->url_slug) ? str_slug($request->url_slug) : str_slug($request->title);
        $formattedForm['meta_title'] = ($request->meta_title) ? $request->meta_title : $request->title;
        $formattedForm['meta_description'] = ($request->meta_description) ? strip_tags($request->meta_description) : strip_tags(mb_substr(iconv('utf-8',
            'utf-8', html_entity_decode(str_replace('<br>', ' ', $request->body1), ENT_QUOTES, 'UTF-8')), 0, 300,
            'utf-8'));
        $formattedForm["language"] = array_get($request, 'language', 'en');
        $formattedForm["visibility"] = array_get($request, 'visibility', 1);
        $formattedForm["vat"] = str_replace('%', '', array_get($request, 'vat', 0));
        $formattedForm["currency"] = array_get($request, 'currency', config('system_config.default_currency'));
        $formattedForm["discount"] = str_replace('%', '', array_get($request, 'discount', 0));

        $formattedForm["trainings_title"] = array_get($request, 'title', null);
        $formattedForm["trainings_subtitle"] = array_get($request, 'subtitle', null);
        $formattedForm["trainings_instructor"] = array_get($request, 'instructor', null);
        $formattedForm["trainings_category_id"] = array_get($request, 'category', 1);
        $formattedForm["trainings_feature_type"] = array_get($request, 'feature_type', 'img');//img or vdo
        $formattedForm["trainings_feature_video"] = array_get($request, 'feature_video', null);
        $formattedForm["trainings_body1"] = array_get($request, 'body1', null);
        $formattedForm["trainings_body2"] = array_get($request, 'body2', null);
        $formattedForm["trainings_price_wo_vat"] = array_get($request, 'price_wo_vat', null);
        $formattedForm["trainings_price_with_vat"] = array_get($request, 'price_with_vat', null);
        $formattedForm["trainings_video_count"] = array_get($request, 'video_count', null);
        $formattedForm["trainings_document"] = array_get($request, 'document', null);
        $formattedForm["trainings_video_length"] = array_get($request, 'video_length', null);
        $formattedForm["trainings_benefit"] = array_get($request, 'benefit', null);
        $formattedForm["trainings_course_duration"] = array_get($request, 'course_duration', null);

        //setup image path
        $imgPath = $this->processUpload($request, 'featured_image', 'trainings');

        if ($imgPath) {
            $formattedForm["trainings_featured_image"] = $imgPath;
        }
        //owner of the training
        return $formattedForm;
    }
    /**
     * @param Request $request
     * @param $storedTraining
     */
    protected function processReccommendedTrainings(Request $request, $storedTraining): void
    {
        $related = $request->get('repeater-group-related', []);
        $relatedList = [];
        foreach ($related as $val) {
            if ($val['related_trainings'] != 0) {
                $relatedList[] = $val['related_trainings'];
            }
        }

        $storedTraining->reccomendedTrainings()->sync($relatedList);
    }

    /**
     * @param Request $request
     * @return array|null|string
     */
    protected function processTags(Request $request)
    {
        $tags = $request->input('tags', []);

        foreach ($tags as $key => $tag) {
            if (is_string($tag)) {

                $data = [
                    'label' => $tag,
                    'model' => $this->eventmodel,
                    'author' => \Auth::id(),
                    'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
                ];

                $condition = [
                    'label' => $tag,
                    'model' => $this->eventmodel,
                ];

                $storedTag = $this->trainings->createNewTag($data, $condition);
                $tags[$key] = $storedTag->id;
            }
        }
        return $tags;
    }


    public function searchItem(Request $request)
    {
        $param = $request->input('term');

        $page = $this->trainings->getTrainingByName(array_get($param, 'term', ''));

        $page = $page->map(function ($page) {

            $page->id = $page->trainings_id;
            $page->text = $page->trainings_title;

            return collect($page->toArray())
                ->only(['id', 'url_slug', 'text'])
                ->all();
        });
        return response()->json($page);
    }
}
