<?php

namespace Creativehandles\ChTrainings\Http\Controllers\PluginsControllers;


use App\Http\Controllers\AdminControllers\UsersController;
use App\Http\Controllers\APIControllers\CoreControllers\UsersController as ApiUsersController;
use App\Http\Resources\UserResource;
use Creativehandles\ChTrainings\Plugins\Trainings\Models\InstructorModel;
use Creativehandles\ChTrainings\Plugins\Trainings\Trainings;
use App\Repositories\CoreRepositories\Contracts\RoleRepositoryInterface;
use App\Repositories\CoreRepositories\Contracts\UserRepositoryInterface;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Instructor;

use DB;
use Spatie\Permission\Models\Role;
use Validator;

class InstructorsController extends UsersController
{

    use UploadTrait;

    protected $views = 'Admin.Instructors.';
    protected $logprefix = "InstructorsController";

    protected $backRoute = "admin.instructors.index";
    /**
     * @var Trainings
     */
    private $trainings;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var UsersController
     */
    private $usersController;

    public function __construct(
        ApiUsersController $apiController,
        UserRepositoryInterface $userRepository,
        RoleRepositoryInterface $roleRepository,
        Trainings $trainings
    ) {
        parent::__construct($apiController, $userRepository, $roleRepository);
        $this->trainings = $trainings;
        $this->userRepository = $userRepository;
    }

    public function grid(Request $request)
    {

        $searchColumns = ['users.id', 'user_details.user_id', 'user.email','user_details.first_name','user_details.last_name','users.avatar','users.rank'];
        $orderColumns = ['1' => 'users.id', '2' => 'user_details.user_id'];
        $with = ['userDetails', 'role'];
        $where = ['users.role_id', '=', Role::findByName('Instructor')->id];
        $data = $this->userRepository->getDataforDataTables($searchColumns, $orderColumns, '2', $with, $where);

        $requiredUsers = config('users.required', []);    // need to disable deleting required users for the app

        $data['data'] = array_map(function ($item) use ($requiredUsers) {
            $return = [
                $item['id'],
                $item['user_details']['user_id'],
                $item['email'],
                $item['user_details']['first_name'],
                $item['user_details']['last_name'],
                $item['avatar'] ?? null,
                $item['rank'] ?? ''
            ];
            $return[] = !in_array($return[2], $requiredUsers, true); // delete is allowed if not a required user

            return $return;
        }, $data['data']);

        echo json_encode($data);

    }


    public function update(Request $request)
    {
        $request->merge(['username'=>$request->email,'role_id'=>Role::findByName('Instructor')->id]);
        return parent::update($request);

    }

    public function store(Request $request)
    {
        $request->merge(['username'=>$request->email,'role_id'=>Role::findByName('Instructor')->id]);
        return parent::store($request);
    }

    public function edit($user)
    {
        $user = $this->userRepository->find($user);
        return view($this->views . 'add', [
            'model' => $user,
        ]);
    }



}
